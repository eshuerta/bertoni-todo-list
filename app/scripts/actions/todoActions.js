import axios from 'axios';
import {server_port, server_domain, server_protocol } from '../../../config/general.config';


const serverUrl = `${server_protocol}://${server_domain}:${server_port}`;

export function getTodos() { 
    return new Promise(resolve => {
        
        axios.get(`${serverUrl}/todos`).then(function (response) {
            resolve(response.data || []);
        });
    });
}

export function addTodo(todoText) {
    
    return new Promise(resolve => {      
        let params = {todoText: todoText};

        axios.post(`${serverUrl}/todos`, params).then(function (response) {
            resolve(response.data);
        }).catch(function (error) {
            console.log(error);
        });
    });
}

export function deleteTodo(id) {
    
    return new Promise(resolve => {      
        let params = {id: id};
        
        axios.delete(`${serverUrl}/todos/${id}`, params).then(function (response) {
            resolve(response.data);
        }).catch(function (error) {
            console.log(error);
        });
    });
}

export function updateTodo(id, value) {
    
    return new Promise(resolve => {      
        let params = {value: value};
        axios.put(`${serverUrl}/todos/${id}`, params).then(function (response) {
            resolve(response.data);
        }).catch(function (error) {
            console.log(error);
        });
    });
}
