/**
 * This file will hold the Menu that lives at the top of the Page, this is all rendered using a React Component...
 * 
 */
import React from 'react';

import TodoItems from './TodoItems';
import { getTodos, addTodo, deleteTodo, updateTodo  } from '../actions/todoActions';

class TodoContainer extends React.Component {

    /**
     * Main constructor for the TodoContainer Class
     * @memberof TodoContainer
     */
    constructor() {
        super();
        this.state = {
           todoList: [],
           newTodoItemText: '',
           messageInput: {show: false, text: ''}
        };

        this.addItem = this.addItem.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
        this.updateItem = this.updateItem.bind(this);
        
    }

    componentDidMount() {
        getTodos().then(data => {
            this.setState({ todoList: data });
        });
    }

    addItem(e) {
        e.preventDefault();
        const todoValue = this.state.newTodoItemText.trim();
        if (todoValue !== '') {
            addTodo(todoValue).then(data => {
                this.setState({ todoList: data, newTodoItemText: ''  });
            });
        } else {
            this.setState({ messageInput: { show: true, text: 'You must enter a value'} });
        }
        
    }

    deleteItem(id) {
        if (id !== '') {
            deleteTodo(id).then(data => {
                this.setState({ todoList: data });
                console.log("resposne data", data);
            });
        } else {
            this.setState({ messageInput: { show: true, text: 'Something went wrong..'} });
        }
    }

    updateItem(id, value) {
        console.log(value);
        if (id !== '') {
            updateTodo(id, value).then(data => {
                this.setState({ todoList: data });
            });
        } else {
            this.setState({ messageInput: { show: true, text: 'Something went wrong..'} });
        }
    }

    render() {

        const {
            todoList,
            messageInput,
            newTodoItemText
        } = this.state;

        return (
            <div className="todoListMain">
                <div className="header">
                    <form onSubmit={this.addItem}>
                        <input
                            onChange={(e) => this.setState({ newTodoItemText: e.currentTarget.value })}
                            placeholder="enter new TODO item"
                            value={newTodoItemText}>
                        </input>
                        <button type="submit">add</button>
                        <p>{messageInput.show && messageInput.text}</p>
                    </form>
                </div>
               <TodoItems todos={todoList} delete={this.deleteItem} update={this.updateItem} />
            </div>
        );
    }


}

// Export out the React Component
module.exports = TodoContainer;