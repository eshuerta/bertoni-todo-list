import React, { Component } from 'react';

class TodoItems extends Component {
  constructor(props) {
    super(props);

    this.createTasks = this.createTasks.bind(this);
  }

  createTasks(item) {
    return  <li key={item.id}>
              <input
                type="checkbox"
                checked={item.isCompleted}
                onChange={ (e) => this.props.update(item.id, e.target.value)}
              />
              {item.text} <span onClick={() => this.props.delete(item.id)} style={{ color: 'red'}}> x</span>
             
            </li>
  }

  render() {
    const {todos} = this.props;
    const listItems = todos.map(this.createTasks)

    return <ul className="theList">{listItems}</ul>
  }
}
export default TodoItems