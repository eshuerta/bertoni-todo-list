
import React from 'react';
import PropTypes from 'prop-types';

class SearchResults extends React.Component {

    /**
     * Main constructor for the SearchResults Class
     * @memberof SearchResults
     */
    constructor(props) {
        super(props);
        this.state = {
            showingResults: this.props.showingResults,
            limitResults: 4 ,
            showAll: false,
            showingSearch: this.props.showingSearch 
        };
    }

    componentWillUpdate(nextProps) {
        if (nextProps.showingResults !== this.state.showingResults)
            this.setState({ showingResults: nextProps.showingResults });

        if (nextProps.showingSearch !== this.state.showingSearch)
            this.setState({ showingSearch: nextProps.showingSearch });
    }

    showSearchContainerResults(e) {
        e.preventDefault();
        this.setState({
            showingResults: !this.state.showingResults
        });
    }

    showHideAll() {
        this.setState({ showAll: !this.state.showAll })
    }

    render() {

        const {searchResults} = this.props;
        const {limitResults, showingResults, showAll, showingSearch } = this.state;
        
        // slice the array result to show X items if results total are less than x and !showAll, if not, leave the result as is
        let itemsToShow = searchResults.length > limitResults && !showAll 
            ? searchResults.slice(0, limitResults)
            : searchResults;
            
        const headerText = `Displaying ${showAll ? searchResults.length : limitResults} of ${searchResults.length } results - `;

        return (
        <div
            className={
                (showingResults && itemsToShow.length > 0 ? "showing " : "") +
                (showingSearch && "searchHidden") +
                " search-results-container "
            }
        >
            {// If the ammount of results is larger than the items in display, show message and link
                searchResults.length > limitResults &&
                <div className="items-header">
                    {headerText}
                    <a href="javascript: void(0);" onClick={ () => this.showHideAll()}> {showAll ? "Hide" : "See"} all results</a>
                    <a href="#" onClick={(e) => this.showSearchContainerResults(e)} className="close-search-results-container">
                        <i className="material-icons close">close</i>
                    </a>
                </div>
            }
            <div className={"items-containter " + (showAll ? "show-all" : "show-less")}>
                {itemsToShow.map(element =>             
                    <div className="product-item" key={ element.id }>
                        <img className="" alt={ element.name } src={ element.picture } />
                        <div className="info-container">
                            <h2>{ element.name }</h2>
                            <p>
                                { element.about }
                            </p>
                        </div>
                    </div>
                )} 
            </div>                  
        </div>
    )}
}

SearchResults.propTypes = {
    showingResults: PropTypes.bool,
    showingSearch: PropTypes.bool,
    searchResults: PropTypes.array
};

// Export out the React Component
module.exports = SearchResults;