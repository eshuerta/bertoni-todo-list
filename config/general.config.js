"use strict";
/**
 * 
 * File is used to setup the General project
 *  
 */

module.exports = {

    development_folder: ".local_server",

    production_folder: "prod",

    image_folder: "img",

    dev_port: 3030,
    server_protocol: 'http',
    server_port: 3035,
    server_domain: 'localhost'
};