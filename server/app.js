const express = require('express'),
    app = express(),
    todos = require('./todos');
    bodyParser = require('body-parser');

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3030');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    next();
});
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());
// app.get('/todos', todos.search);

var todoData = [
    {
        id: 1,
        text: "Initiate proyect",
        isCompleted: true
    },
    {
        id: 2,
        text: "NPM Install",
        isCompleted: false
    },
    {
        id: 3,
        text: "Create TODO list",
        isCompleted: false
    }
];

app.get("/todos", (req, res) => {
    res.json(todoData);
});

app.post('/todos', (req, res) => {
    console.log(req.body);

    if (req.body && req.body.todoText) {
      todoData.push({
        id: Date.now(),
        text: req.body.todoText,
        isCompleted: false
      });
    } else {
      res.status(500).send({ error: "Todo item could not be added..." });
    }

    console.log(todoData);
    res.status(200).json(todoData);
    
});

app.delete('/todos/:id', function (req, res) {
    
    let id = req.params.id;
    todoData = todoData.filter(function( item ) {
        return item.id !=id;
    });
   
    return res.status(200).json(todoData);
    
});

app.put('/todos/:id', function (req, res) {

    let id = req.params.id;
    
    objIndex = todoData.findIndex((item => item.id == id));
    if (objIndex) {
      todoData[objIndex].isCompleted =  req.body.value;
      return res.status(200).json(todoData);
    } else {
      return res.status(500).send({ error: "Todo item could not be edited..." });
    }
    
});


app.listen(3035);
console.log('Listening on port 3035...');